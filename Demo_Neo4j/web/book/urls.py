from django.conf.urls import url, include 
from django.contrib import admin 
from django.views.generic import TemplateView
from django.urls import path
from Book import views

app_name ='Book'
urlpatterns = [
    path('index',views.index),
    path('',views.Mylogin,name ='login'),
    path('logout',views.Mylogout, name='logout'),
    path('register',views.MyRegister, name = 'register'),
]
