from django.db import models
from neomodel import StructuredNode,StringProperty, UniqueIdProperty, DateProperty, EmailProperty
from django_neomodel import DjangoNode
class Book(DjangoNode):
    title = StringProperty(unique_index=True)
    published = DateProperty()
    class Meta:
        app_label = 'Book'

class Writer(DjangoNode):
    name = StringProperty(unique_index=True)
    birthday = DateProperty()
    class Meta:
        app_label = 'Book'
    
class User(DjangoNode):
    ID = UniqueIdProperty()
    fullname =StringProperty()
    username = StringProperty()
    password = StringProperty()
    email = EmailProperty(unique_index = True)
    class Meta:
        app_label = 'Book'