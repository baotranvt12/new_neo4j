from os import name
from django.shortcuts import redirect, render
from .models import Book
from django.contrib.auth import logout
# Create your views here.

def index(request):
    return render(request,'Book/index.html',{'Book':Book.nodes.all()})

def Mylogin(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = User.nodes.get_or_none(username=username)
        
        if user == None:
            return render(request,'Book/login.html',{'message':'Tài khoản của bạn không tồn tại'})
        else:
            if user.password == password:
                return render(request,'Book/index.html',{'message':user.fullname})
            else:
                 return render(request,'Book/login.html',{'message':'mật khẩu của bạn không đúng'})
    return render(request,'Book/login.html')

def Mylogout(request):
    try:
        logout(request)
    except:
        pass
    return redirect('Book:login')

def MyRegister(request):
    if request.method == 'POST':
        fullname = request.POST.get('fullname')
        username = request.POST.get('username')
        password = request.POST.get('password')
        password2 = request.POST.get('password2')
        print(password2,password)
        user = User.nodes.get_or_none(username=username)
        if user == None:
            if password == password2:
                new_user = User(username = username,password =password, fullname = fullname)
                new_user.save()
                return redirect('Book:login')
            else:
                return render(request,'Book/register.html',{'message':'mật khấu xác nhận bạn không đúng'})
        else:
            return render(request,'Book/register.html',{'message':'tài khoản đã tồn tại'})
    return render(request,'Book/register.html')